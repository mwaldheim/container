# Gitlab CI Container

This project contains various Gitlab CI containers and pipeline fragments, which can be used for your pipeline. can be
used. In the standard we assume the following stages, on which these are based:

> **IMPORTANT**: Optimize all your projects to [PNPM](https://pnpm.io/pnpm-cli) for better cache controlling and reduce your installation times. All 

```yaml
stages:
  - build
  - test
  - deploy
  - review
  - dast
  - staging
  - canary
  - production
  - performance
  - cleanup
```

Currently, the following container fragments are available. All pipelines are updated automatically and do not require
any update from you:

| Titel            | Beschreibung                                                                | Implementierung                                                                         |
|:-----------------|-----------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|
| Cypress          | Used to run Cypress tests in parallel with connection to Cypress cloud      | ```remote: https://gitlab.com/mwaldheim/container/-/raw/main/cypress/cypress.yml```     |
| Firebase         | Used to make hosting releases for Firebase.                                 | ```remote: https://gitlab.com/mwaldheim/container/-/raw/main/firebase/firebase.yml```   |
| Semantic-Release | Used to automatically version your software according to Angular standards. | ```remote: https://gitlab.com/mwaldheim/container/-/raw/main/version/release.yml```     |
| Angular          | Used to check, if ng test and ng lint run without an error                  | ```remote: https://gitlab.com/mwaldheim/container/-/raw/main/angular/angular.yml```     |
| SSH Deploy       | Used to update a git-based docker-Project on an remote server               | ```remote: https://gitlab.com/mwaldheim/container/-/raw/main/sshdeploy/sshdeploy.yml``` |

All steps are easily built into the Gitlab pipeline. For this you only have to insert the following passage in
the `.gitlab-ci.yml`. and make an entry for each pipeline if you want to use it.

```yaml
include:
  - [ NAME_OF_YAMLFILE ]

```

Example:

```yaml
include:
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/version/release.yml
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/firebase/firebase.yml
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/cypress/cypress.yml
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/angular/angular.yml
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/gitbook/gitbook.yml
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/sshdeploy/sshdeploy.yml
  - remote: https://gitlab.com/mwaldheim/container/-/raw/main/nx.dev/nx.dev.yml (COMING SOON)
```

## Version Configuration

If you want to use the version control, you have to place a config file in the root directory which generates the
changelogs. The standards of Semantic-Release apply.

Copy the file [/version/.releserc.yml](https://gitlab.com/mwaldheim/container/-/blob/main/version/.releserc.yml) into
your project.

Copy the file `zip.js` and check that the name of your project into dist is the same as the name in your package.json

## Angular Configuration

To run Angular's processes, the following processes must be included in the package.json. What they do in it is again up
to you.

```json
{
  scripts: {
    "ng": "ng",
    "start": "ng serve",
    "build": "ng build",
    "test": "ng test",
    "lint": "ng lint",
    "e2e": "ng e2e"
  }
}
```

**Relevant scripts**

| Script | Description                                       |
|:-------|:--------------------------------------------------|
| `lint` | Call something like eslint to check to Code       |
| `test` | Call something like jest / karma to test the Code |

## Cypress Configuration

To use Cypress, you need a Cypress account. As CI variable `$CYPRESS_RECORD_KEY` must be defined then. Also, you need
test:e2e:chrome, test:e2e:edge, test:e2e:firefox with the
Cypress-Calls `npx cypress run --record --key $CYPRESS_RECORD_KEY --parallel --browser $BROWSER --group 'UI - $BROWSER'`.
The dependencies `start-server-and-test` must be installed.

## Firebase Configuration

To use Firebase, you need a Firebase account. As CI variable `$FIREBASE_TOKEN` must be defined then.
Copy the follow files into your Root directory:

| File Name     | Description                                                       |
|:--------------|:------------------------------------------------------------------|
| .firebaserc   | Contains the name of your Firebase project. Replace `$PROJECT_ID` |
| firebase.json | Contains Settings of your Structur. Replace `$FOLDER`             |
| prebuild.mjs  | Contains an generic prebuild script.                              |

## nx.dev Configuration

To use nx.dev, you need a nx.dev account. To Run Process you need to install the nx.dev cli.

## SSH Deploy Configuration

To use SSH Deploy, you need a SSH Key. As CI variable `$SSH_KEY` must be defined then.
Also, you need to define the following variables:

| Variable Name | Description                                                      |
|:--------------|:-----------------------------------------------------------------|
| $SSH_HOST     | The Hostname of the Server                                       |
| $SSH_USER     | The Username of the Server                                       |
| $SSH_PORT     | The Port of the Server                                           |
| $SSH_PATH     | The Path of the Project on the Server                            |
| $SSH_KEY      | The SSH Key of the Server. This is a Base64 encoded private key. |

## SFTP Deploy Configuration

to use SFTP Deploy, you need to Config some Informations as CI variables. 

| variable Name | Description |
| :------------ | :---------- |
| SFTP_USER | Username of Hostsserver |
| SFTP_PASSWORD | Password of User |
| SFTP_HOST | Hostname |
| SFTP_PORT | Port of the Server | 
| SFTP_REMOTE_DIR | remote-Directory to Upload |
| LOCAL_DIR | Local Path to Upload |
